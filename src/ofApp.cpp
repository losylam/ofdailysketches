#include "ofApp.h"

vector<float> freq;
vector<float> delta;
vector<float> v_vel_y;
vector<int> strips;

vector<ofColor> cols;

float vel_x, vel_y;

float orig = 127;
float amp = 127;
int x_step;

float width_line= 40;
float r1, r2;

//--------------------------------------------------------------
void ofApp::setup(){
  ofSetVerticalSync(true);
  ofSetLineWidth(5);


  for (int i = 0; i < 3; i++){
    cols.push_back(ofColor(ofRandom(0, 255), ofRandom(0, 255), ofRandom(0, 255)));
  }

  cout << cols.size() << endl;

  for (int i = 0; i < 1000; i++){
    freq.push_back(ofRandom(10.0, 10.0));
    delta.push_back(ofRandom(-4.0, 4.0));
    v_vel_y.push_back(ofRandom(150, 400));
    strips.push_back(int(ofRandom(2, 10)));
  }
  ofNoFill();
}

//--------------------------------------------------------------
void ofApp::update(){
  ofBackground(0);
  ofSetCircleResolution(100); 

}

//--------------------------------------------------------------
void ofApp::draw(){

  width_line = 10 + 15 * (1+sin(ofGetElapsedTimef()*0.89/2.0));
  vel_x = 75;//ofMap(mouseX, 0, ofGetWidth(), 0, 100.0);
  vel_y = 50;//ofMap(mouseY, 0, ofGetHeight(), 0, 100.0);

  // float orig = 127;
  // float amp = 127;

  // //  float delta = ofMap(mouseY, 0, ofGetHeight(), -0.02, 0.02);
  // float delta = 0.001;


  // for (int i = 100; i < ofGetWidth()-100; i=i+1){
  //   //    ofSetColor(orig + amp * sin((500+i)*(0.01+2*delta) + t), orig + amp * sin((500+i)*(0.01+delta) + t), orig + amp * sin((500+i)*(0.01) + t)); 
  //   ofSetColor(255);
  //       float sc = 0.25 + sin(ofMap(i, 100, ofGetWidth()-100, 0, PI));
  //   ofDrawCircle(i,
  // 		 ofGetHeight()/2 + 100 * sin(t + i/vel_x),
  // 		 ((40 + 25 * sin((t*0.35)+i/vel_y)) + (10 + 10 * sin((t*0.52)+i/vel_y )))*sc
  // 		 );
  // }

  // for (int i = 100; i < ofGetWidth()-100; i=i+1){

  //   float sc = 0.25+sin(ofMap(i, 100, ofGetWidth()-100, 0, PI));
  //   //ofSetColor(orig + amp * sin((500+i)*0.01 + t), orig + amp * sin((500+i)*(0.01+delta) + t), orig + amp * sin((500+i)*(0.01 + 2*delta) + t));
  //   ofSetColor(0);
  //   ofDrawCircle(i,
  // 		 ofGetHeight()/2 + 100 * sin(t + i/vel_x),
  // 		 (40 + 25 * sin((t*0.35)+i/vel_y)) * sc
  // 		 );

  // }

  r2 = ofMap(mouseX, 0, ofGetWidth(), 0, 100);
  r1 = ofMap(mouseY, 0, ofGetWidth(), 0, 100);

  int n_lignes = 20;

  //  x_step = ofMap(mouseX, 0, ofGetWidth(), 40, 1);
  x_step = 2;
  for (int i = n_lignes; i > 0; i--){
    // if (i%2 == 0){
    //   ofSetColor(255);
    // }else{
    //   ofSetColor(0);
    // }
    drawSine(i);
  }
}
 
void ofApp::drawSine(int num){

  float dif = 1;

  float t = ofGetElapsedTimef();
  for (int i = -2000; i < ofGetWidth()+500 ; i=i+x_step){

    //ofSetColor(orig + amp * sin((250+i)*0.01 + t*tt), orig + amp * sin((250+i)*(0.01+deltaa) + t*tt), orig + amp * sin((250+i)*(0.01 + 2*deltaa) + t*tt));


    float ii = i;
    // if (num%2 == 0){
    //   ii = ofGetWidth()+500-i;
    // }
    float r = 0;

    if ( (int(ii/x_step + t*40)) % 8 == 0){
      ofSetColor(255);
    }else{
      //	ofSetColor(orig + amp * sin((250+i)*0.01 + t*tt), orig + amp * sin((250+i)*(0.01+deltaa) + t*tt), orig + amp * sin((250+i)*(0.01 + 2*deltaa) + t*tt));
      ofSetColor(0);
    }

    float pos;

    for(int j = 0; j<num; j++){

      pos =  t*freq[j] + ii/v_vel_y[j];
      r +=  dif*(r1 + r2 * (1+sin(pos))/2.0 * (1+sin(t*delta[j])/2.0));
    }
    // if (ii+r < ofGetWidth()-100 && ii-r  > 100){
    //   if (y+r < ofGetWidth()-100 && y-r  > 100){
    //	ofDrawRectangle(ii-r/2.0, y-r/2.0 , r, r);

    pos =  t*2 + ii*(0.5+0.5*sin(t*0.2))/vel_y;
    float y = ofGetHeight()/2  + 35 * sin(pos) * (1+sin(t)/2.0) + 250 * sin(t);

    ii = ii + 2*width_line*num;

    //    ofColor c = ofColor(cols[num % cols.size()]);
    ofColor c = ofColor(cols[0]);
    float h = c.getHue();
    ofColor cc = ofColor(c);
    cc.setHue(int(h + num*3)%255);

    rectCol(ii, y , r, cc, pos);
 
    //   }
    // }
  }
}

void ofApp::rectCol(float x, float y, float r, ofColor col, float pos){
  float h = col.getHue();
  ofColor col1 = ofColor(col);
  col1.setBrightness(255);
  ofSetColor(col1);
  ofDrawLine(x-width_line, y-r/2.0, x-width_line, y+r/2);


  ofColor col2 = ofColor(col);
  //  col2.setBrightness(90 + 100*cos(pos));
  col2.setBrightness(60);
  ofSetColor(col2);

  ofDrawLine(x-width_line, y-r/2.0, x+width_line, y-r/2);
  
  ofColor col3 = ofColor(col);
  //col3.setBrightness(130 - 100 * cos(pos));
  col3.setBrightness(120);
  ofSetColor(col3);

  ofDrawLine(x-width_line, y+r/2.0, x+width_line, y+r/2);

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
  freq.clear();
  delta.clear();
  v_vel_y.clear();
  strips.clear();

  cols.clear();

  int n_cols = ofRandom(3, 6);
  for (int i = 0; i < n_cols; i++){
    cols.push_back(ofColor(ofRandom(0, 255), ofRandom(0, 255), ofRandom(0, 255)));
  }

  
  for (int i = 0; i < 1000; i++){
    freq.push_back(ofRandom(-4.0, 4.0));
    delta.push_back(ofRandom(-4, 4));
    v_vel_y.push_back(ofRandom(200, 400));
    strips.push_back(int(ofRandom(3, 10)));
  }

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
